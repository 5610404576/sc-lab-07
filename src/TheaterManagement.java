import java.util.ArrayList;



public class TheaterManagement {
	
	private double [][] ticketPrices ;
	private int date;
	private ArrayList<double[][]> seatRound; 
	
	public TheaterManagement(ArrayList<double[][]> seatRound){
		this.seatRound = seatRound;
		
	}
	public void setDate(int date){
		this.date = date;
	}
	public int getDate(){
		return date;
	}
	
	public int getRollInteger(String rollStr){
		int i = rollStr.toUpperCase().charAt(0);
		i = i - 65;
		return i;
	}
	
	public double buySeat(int roundshow, String rollStr , int seat){
		ticketPrices = seatRound.get(roundshow-1);
		seat = seat - 1;
		double prices = -1;
		int roll = getRollInteger(rollStr);
		if( ticketPrices[roll][seat] > 0 ){
			prices = ticketPrices[roll][seat];
			ticketPrices[roll][seat] = 0;
		}
		else if( ticketPrices[roll][seat] == 0 ){
			prices = 0;
		}
		return prices;
	}
	
	public double[][] getTicketPrices() {
		return ticketPrices;
	}
	
	public int getRound(int date){
		int round;
		if(date == 7 || date == 1){
			round = 3;
		}
		else {
			round = 2;
		}
		return round;
	}
	
	public String getRollStr(int roll){
		String rollStr = "";
		roll = roll + 65;
		char c = (char) roll;
		rollStr = rollStr + c;
		return rollStr;
	}
	
	public String searchSeatBySeatPrices(int roundshow,double seatPrices){
		ticketPrices = seatRound.get(roundshow-1);
		String s = "";
		for(int i = 0 ; i < ticketPrices.length ; i++){
			String line = "";
			line = line + getRollStr(i)+" ";
			int count = 0;
			for(int j = 0 ; j < ticketPrices[i].length ; j++ ){
				if(  ticketPrices[i][j] == seatPrices){
					line = line + (j+1) +" ";
					count = 1;
				}
			}
			if(count == 1){
				line = line+"\n";
				s = s+line;
			}
		}
		return s;
	}
	
	public void showAllSeat(int roundshow){
		ticketPrices = seatRound.get(roundshow-1);
		String seatNumberLine = "   ";
		for(int i = 0 ; i < ticketPrices[0].length ; i++){
			seatNumberLine = seatNumberLine+(i+1)+" ";
			if( i+1 < 10){
				seatNumberLine = seatNumberLine+" ";
			}
		}
		System.out.println(seatNumberLine);
		for(int i = 0 ; i < ticketPrices.length ;i++){
			String line = getRollStr(i)+"  ";
			for(int j = 0 ; j < ticketPrices[i].length ; j++){
				if( ticketPrices[i][j] > 0){
					int a = (int)ticketPrices[i][j];
					line = line+a+" ";
				}
				else if( ticketPrices[i][j] == 0){
					line = line+"0  ";
				}
				else{
					line = line+"   ";
				}
			}
			System.out.println(line);
			
		}
	}
}
	
