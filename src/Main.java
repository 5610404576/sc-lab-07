import java.util.Calendar;


public class Main {

	public static void main(String[] args) {
		DataSeatPrice ds = new DataSeatPrice();
		Seat seat = new Seat();
		TheaterManagement tm = new TheaterManagement(seat.SeatRound());
		Calendar car = Calendar.getInstance();
		int date = car.get(Calendar.DAY_OF_WEEK);
		tm.setDate(date);
		System.out.println("Amount Round: "+tm.getRound(date));
		System.out.println("round:1 O11 Price: "+tm.buySeat(1,"O", 11));
		System.out.println("round:1 O12 Price: "+tm.buySeat(1,"O", 12));
		System.out.println("\nround:1 Search Seat Price: 20");
		System.out.println(tm.searchSeatBySeatPrices(1,20));
		System.out.println("round:1 Search Seat Price: 50");
		System.out.println(tm.searchSeatBySeatPrices(1,50));
		System.out.println("round:1 O12 Price: "+tm.buySeat(1,"O", 12));
		System.out.println("round:2 O13 Price: "+tm.buySeat(2,"O", 13));
		System.out.println("\nround 1: \n");
		tm.showAllSeat(1);
		System.out.println("\nround 2:\n");
		tm.showAllSeat(2);
	}

}
